A STATA program which implements an extension to the Kitagawa-Oaxaca-Blinder decomposition to repeated cross-sectional or panel data.

For more information, please read the preliminary version of the paper:  https://osf.io/preprints/socarxiv/egj79/

# Installation 

```bash
git clone https://gitlab.com/jhart/xtoaxaca
```
Then copy the files into your STATA ado folder into the x folder.

# Examples

## Example 1: Interventionist decomposition of income trajectories:

```stata
webuse set https://gitlab.com/jhart/xtoaxaca/-/raw/master/
webuse xtoaxaca_example, clear

xtset id time

* Note that full factor-variable notation is used
  xtreg inc i.group##i.time##i.edu c.exp##i.group##i.time

est store control

xtoaxaca exp edu, groupvar(group) groupcat(0 1) timevar(time) times(4 2 1)  model(control) timeref(2) change(kim) 
```

## Example 2:  Example 2: Contribution of an intervention to change in group differences

```stata
webuse set https://gitlab.com/jhart/xtoaxaca/-/raw/master/
webuse xtoaxaca_example2, clear

xtset id time

qui: eststo cont: xtreg dep i.time##i.group##i.int1 i.time##i.group##i.int2, i(id)

xtoaxaca int1 int2, groupvar(group) groupcat(0 1) timevar(time) times(1 2) ///
                    timeref(1)  model(cont) change(interventionist)
```

## Example 3: Interactions of decomposition variables

```stata
webuse set https://gitlab.com/jhart/xtoaxaca/-/raw/master/
webuse xtoaxaca_example, clear

* Create interaction term for a squared term
replace exp = exp-10
gen exp2 = exp*exp

* for interactions among decomposition variables (squared experience)
* a hand-made interaction-term is used
qui: eststo est3: xtreg inc i.group##i.time##c.(exp exp2) i.group##i.time##i.edu

xtoaxaca edu exp exp2, groupvar(group) groupcat(0 1) timevar(time) nolevels ///
         times(4 2 1) model(est3) timeref(2) change(kim) detail normalize(edu)
```


# Howto use with Multiple Imputation
```stata
cap program drop collect_summary
program define collect_summary 
	syntax, mat_b(string) mat_v(string) years(numlist)

	local rows_level_summary levels_Yobs_ levels_E_ levels_C_ levels_CE_ levels_reffects_ levels_Total_ 
	local rows_change_summary change_Yobs_ change_E_ change_C_ change_CE_  change_RE_ change_Total_ 
	local types level change 
	
	foreach i of numlist `years' {
		foreach type of local types {
			foreach row of local rows_`type'_summary {
				 qui: local b = `mat_b'[1, "`row'`i'"]
				 qui: collect, tag(varname[`row'] time[t`i'] type[`type'] p[b]): echo `b'
				 qui: local se = sqrt(`mat_v'["`row'`i'","`row'`i'"])
				 qui: collect, tag(varname[`row'] time[t`i'] type[`type'] p[se]): echo `se'
				 qui: local z = `b' / `se'
				 qui: collect, tag(varname[`row'] time[t`i'] type[`type'] p[z]): echo `z'
				 qui: local p = 1-normal(`z')
				 qui: collect, tag(varname[`row'] time[t`i'] type[`type'] p[p]): echo `p'
				 qui: local star ""
				 qui: if (`p' < 0.05) local star "*"
				 qui: if (`p' < 0.01) local star "**"
				 qui: if (`p' < 0.001) local star "***"
				 qui: collect, tag(varname[`row'] time[t`i'] type[`type'] p[star]): echo `star'
			}
		}
	}
end

collect clear 

* Compute Coefficient Matrix
local rows = rowsof(B_m1)
local cols = colsof(B_m1) 

matrix B = J(`rows',`cols',0) 

forvalues i=1(1)$M {
	mat B = B + B_m`i'
}
mat B = B / ${M}

* Compute Within Variance Matrix
local rows = rowsof(V_m1)
local cols = colsof(V_m2) 

matrix V_w = J(`rows',`cols',0) 
forvalues i=1(1)$M {
	mat V_w = V_w + V_m`i'
}
mat V_w = V_w / ${M}

* Compute Between Variance Matrix
local dim = rowsof(V_w)
matrix V_b = J(`dim', `dim', 0)

forvalues i = 1/`dim' {
	local v = (B_m1[1,`i'] - B[1,`i']) * (B_m1[1,`i'] - B[1,`i'])
	forvalues m=2(1)$M {
		local v = `v' + (B_m`m'[1,`i'] - B[1,`i']) * (B_m`m'[1,`i'] - B[1,`i'])
	}
	matrix V_b[`i',`i'] = `v' / (${M} 1)	
}

mat V = V_w + V_b + V_b/2
local rownames: colnames B
mat rownames V = `rownames'
mat colnames V = `rownames'

* Collect results
preserve 
drop _all
svmat B, names(eqcol)
save "${dataout}/decomp_res_B`version'.dta", replace
drop _all
svmat V, names(eqcol)
save "${dataout}/decomp_res_V`version'.dta", replace
restore

collect_summary, mat_b(B) mat_v(V) years(1 3 6)

```
